/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup rawfile
 * @{
 *
 * @brief Provides the function of operating rawfile directories and rawfiles.
 *
 * These functions include traversing, opening, searching, reading, and closing rawfiles.
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file raw_file.h
 *
 * @brief Provides functions for operating rawfiles.
 *
 * These functions include searching, reading, and closing rawfiles.
 *
 * @since 8
 * @version 1.0
 */
#ifndef GLOBAL_RAW_FILE_H
#define GLOBAL_RAW_FILE_H

#include <string>

#ifdef __cplusplus
extern "C" {
#endif

struct RawFile;

/**
 * @brief Provides the function of accessing rawfiles.
 *
 *
 *
 * @since 8
 * @version 1.0
 */
typedef struct RawFile RawFile;

/**
 * @brief Provides rawfile descriptor information.
 *
 * <b>RawFileDescriptor</b> is an output parameter of {@link OH_ResourceManager_GetRawFileDescriptor}.
 * It provides information including the rawfile descriptor and the start position and length of the rawfile in the HAP package.
 *
 * @since 8
 * @version 1.0
 */
typedef struct {
    /** rawfile descriptor */
    int fd;

    /** Start position of rawfile in the HAP package */
    long start;

    /** Length of rawfile in the HAP package */
    long length;
} RawFileDescriptor;

/**
 * @brief Reads a rawfile.
 *
 * You can use this function to read data of the specified length from the current position.
 *
 * @param rawFile Indicates the pointer to {@link RawFile}.
 * @param buf Indicates the pointer to the buffer for storing the read data.
 * @param length Indicates the length of the read data, in bytes.
 * @return Returns the length of the read data in bytes. If the length is beyond the end of the rawfile, <b>0</b> will be returned.
 * @since 8
 * @version 1.0
 */
int OH_ResourceManager_ReadRawFile(const RawFile *rawFile, void *buf, size_t length);

/**
 * @brief Seeks for the data read/write position in the rawfile based on the specified offset.
 *
 * @param rawFile Indicates the pointer to {@link RawFile}.
 * @param offset Indicates the specified offset.
 * @param whence Indicates the data read/write position. The options are as follows: \n
 * <b>0</b>: The read/write position is <b>offset</b>. \n
 * <b>1</b>: The read/write position is the current position plus <b>offset</b>. \n
 * <b>2</b>: The read/write position is the end of the file (EOF) plus <b>offset</b>.
 * @return Returns the new data read/write position if the operation is successful; returns <b>(long) -1</b> otherwise.
 * @since 8
 * @version 1.0
 */
int OH_ResourceManager_SeekRawFile(const RawFile *rawFile, long offset, int whence);

/**
 * @brief Obtains the length of a rawfile in int32_t.
 *
 * @param rawFile Indicates the pointer to {@link RawFile}.
 * @return Returns the total length of the rawfile.
 * @since 8
 * @version 1.0
 */
long OH_ResourceManager_GetRawFileSize(RawFile *rawFile);

/**
 * @brief Closes an opened {@link RawFile} and releases all associated resources.
 *
 *
 *
 * @param rawFile Indicates the pointer to {@link RawFile}.
 * @see OH_ResourceManager_OpenRawFile
 * @since 8
 * @version 1.0
 */
void OH_ResourceManager_CloseRawFile(RawFile *rawFile);

/**
 * @brief Obtains the current offset of the rawfile in int32_t.
 *
 * 
 *
 * @param rawFile Indicates the pointer to {@link RawFile}.
 * @return Returns the current offset of the rawfile.
 * @since 8
 * @version 1.0
 */
long OH_ResourceManager_GetRawFileOffset(const RawFile *rawFile);

/**
 * @brief Opens a rawfile descriptor.
 *
 * After the descriptor is opened, you can use it to read the rawfile based on the offset (in int32_t) and file length.
 *
 * @param rawFile Indicates the pointer to {@link RawFile}.
 * @param descriptor Indicates the rawfile descriptor, and the start position and length of the rawfile file in the HAP package.
 * @return Returns <b>true</b> if the rawfile descriptor is opened successfully; returns <b>false</b> if the rawfile cannot be accessed.
 * @since 8
 * @version 1.0
 */
bool OH_ResourceManager_GetRawFileDescriptor(const RawFile *rawFile, RawFileDescriptor &descriptor);

/**
 * @brief Closes a rawfile descriptor.
 *
 * To prevent file descriptor leakage, you are advised to release a rawfile descriptor after use.
 *
 * @param descriptor Indicates the rawfile descriptor, and the start position and length of the rawfile file in the HAP package.
 * @return Returns <b>true</b> if the rawfile descriptor is closed successfully; returns <b>false</b> otherwise.
 * @since 8
 * @version 1.0
 */
bool OH_ResourceManager_ReleaseRawFileDescriptor(const RawFileDescriptor &descriptor);

#ifdef __cplusplus
};
#endif

/** @} */
#endif // GLOBAL_RAW_FILE_H
