/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief Provides unified, standard USB driver APIs to implement USB driver access.
 *
 * Using the APIs provided by the USB driver module, USB service developers can implement the following functions: 
 * enabling/disabling a device, obtaining a device descriptor, obtaining a file descriptor, enabling/disabling an interface, 
 * reading/writing data in bulk mode, setting or obtaining device functions, and binding or unbinding a subscriber.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IUsbInterface.idl
 *
 * @brief Defines standard APIs of the USB driver module.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the USB driver module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.usb.v1_0;

import ohos.hdi.usb.v1_0.UsbTypes;
import ohos.hdi.usb.v1_0.IUsbdSubscriber;
import ohos.hdi.usb.v1_0.IUsbdBulkCallback;

/**
 * @brief Provides APIs to implement basic operations of the USB driver module.
 *
 * The USB service can call related APIs to enable or disable a device, obtain a device descriptor, and 
 * read or write data in bulk mode.
 */
interface IUsbInterface {

    /**
     * @brief Opens a USB device to set up a connection.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    OpenDevice([in] struct UsbDev dev);

    /**
     * @brief Closes a USB device to release all system resources related to the device.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    CloseDevice([in] struct UsbDev dev);

    /**
     * @brief Obtains the USB device descriptor, which provides information about the device, device configuration, and device class.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param descriptor USB device descriptor.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetDeviceDescriptor([in] struct UsbDev dev, [out] unsigned char[] descriptor);

    /**
     * @brief Obtains a string descriptor based on the string ID of a USB device. The string descriptor provides information about 
     * a device interface, such as the vendor name and product SN.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param descId String ID of the USB device.
     * @param descriptor String descriptor of the USB device.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetStringDescriptor([in] struct UsbDev dev, [in] unsigned char descId, [out] unsigned char[] descriptor);

    /**
     * @brief Obtains a configuration descriptor based on the configuration ID of a USB device. The configuration descriptor provides
     * information about the device configuration and interfaces, alternate settings, and endpoints.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param descId Configuration ID of the USB device.
     * @param descriptor Configuration descriptor of the USB device.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetConfigDescriptor([in] struct UsbDev dev, [in] unsigned char descId, [out] unsigned char[] descriptor);

    /**
     * @brief Obtains the raw descriptor of a USB device.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param descriptor Raw descriptor of the USB device.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetRawDescriptor([in] struct UsbDev dev, [out] unsigned char[] descriptor);

    /**
     * @brief Obtains the file descriptor of a USB device.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param fd File descriptor of the USB device.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetFileDescriptor([in] struct UsbDev dev, [out] int fd);

    /**
     * @brief Sets the USB device configuration. After a USB device is configured by the host, the host can use all functions 
     * provided by the device.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param configIndex Index (a numeric string) of the string descriptor for the USB device configuration.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetConfig([in] struct UsbDev dev, [in] unsigned char configIndex);

    /**
     * @brief Obtains the USB device configuration.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param configIndex Index (a numeric string) of the string descriptor for the USB device configuration.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetConfig([in] struct UsbDev dev, [out] unsigned char configIndex);

    /**
     * @brief Claims a USB interface exclusively. This must be done before data transfer.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param interfaceid USB interface ID.
     * @param force Whether to perform the operation forcibly. The value <b>1</b> means to perform the operation forcibly, and 
     * the value <b>0</b> indicates the opposite.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    ClaimInterface([in] struct UsbDev dev, [in] unsigned char interfaceid, [in] unsigned char force);

    /**
     * @brief Releases a USB interface and the resources in use. This is usually done after data transfer.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param interfaceid USB interface ID.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    ReleaseInterface([in] struct UsbDev dev, [in] unsigned char interfaceid);

    /**
     * @brief Sets the alternate settings for the specified interface of a USB device. This allows you to switch between 
     * two interfaces with the same ID but different alternate settings.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param interfaceid USB interface ID.
     * @param altIndex Alternate settings of the USB interface.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     * 
     * @since 3.2
     * @version 1.0
     */
    SetInterface([in] struct UsbDev dev, [in] unsigned char interfaceid, [in] unsigned char altIndex);

    /**
     * @brief Reads data through bulk transfer when the specified endpoint of a USB device is in the data reading direction.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param timeout Timeout duration.
     * @param data Data to read.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    BulkTransferRead([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [out] unsigned char[] data);

    /**
     * @brief Writes data through bulk transfer when the specified endpoint of a USB device is in the data writing direction.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param timeout Timeout duration.
     * @param data Data to write.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    BulkTransferWrite([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [in] unsigned char[] data);

    /**
     * @brief Performs control transfer on a USB device when endpoint 0 is the control endpoint and is in the data reading direction.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param ctrl Control transfer. For details, see {@link UsbCtrlTransfer}.
     * @param data Data to read.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    ControlTransferRead([in] struct UsbDev dev, [in] struct UsbCtrlTransfer ctrl, [out] unsigned char[] data);

    /**
     * @brief Performs control transfer on a USB device when endpoint 0 is the control endpoint and is in the data writing direction.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param ctrl Control transfer. For details, see {@link UsbCtrlTransfer}.
     * @param data Data to write.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    ControlTransferWrite([in] struct UsbDev dev, [in] struct UsbCtrlTransfer ctrl, [in] unsigned char[] data);

    /**
     * @brief Reads data through interrupt transfer when the specified endpoint of a USB device is in the data reading direction.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param timeout Timeout duration.
     * @param data Data to read.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    InterruptTransferRead([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [out] unsigned char[] data);

    /**
     * @brief Writes data through interrupt transfer when the specified endpoint of a USB device is in the data writing direction.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param timeout Timeout duration.
     * @param data Data to write.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    InterruptTransferWrite([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [in] unsigned char[] data);

    /**
     * @brief Reads data through isochronous transfer when the specified endpoint of a USB device is in the data reading direction.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param timeout Timeout duration.
     * @param data Data to read.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    IsoTransferRead([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [out] unsigned char[] data);

    /**
     * @brief Writes data through isochronous transfer when the specified endpoint of a USB device is in the data writing direction.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param timeout Timeout duration.
     * @param data Data to write.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    IsoTransferWrite([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [in] unsigned char[] data);

    /**
     * @brief Sends or receives requests for isochronous transfer on the specified endpoint of a USB device. 
     * The data transfer direction is determined by the endpoint direction.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param clientData Client data.
     * @param buffer Buffer for data transfer.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    RequestQueue([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] unsigned char[] clientData, [in] unsigned char[] buffer);

    /**
     * @brief Waits for the operation result of the isochronous transfer request in <b>RequestQueue</b>.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param clientData Client data.
     * @param buffer Buffer for data transfer.
     * @param timeout Timeout duration.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    RequestWait([in] struct UsbDev dev, [out] unsigned char[] clientData, [out] unsigned char[] buffer, [in] int timeout);

    /**
     * @brief Cancels a data transfer request to be processed.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    RequestCancel([in] struct UsbDev dev, [in] struct UsbPipe pipe);

    /**
     * @brief Obtains the list of functions (represented by bit field) supported by a USB device.
     *
     * @param funcs List of functions supported by the USB device.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    GetCurrentFunctions([out] int funcs);

    /**
     * @brief Sets the list of functions (represented by bit field) supported by a USB device.
     *
     * @param funcs List of functions supported by the current USB device.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetCurrentFunctions([in] int funcs);

    /**
     * @brief Sets the role of a USB device port.
     *
     * @param portId Port ID of the USB device.
     * @param powerRole Power role of the USB device.
     * @param dataRole Data role of the USB device.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    SetPortRole([in] int portId, [in] int powerRole, [in] int dataRole);

    /**
     * @brief Queries the settings of the specified port of a USB device.
     *
     * @param portId Port ID of the USB device.
     * @param powerRole Power role of the USB device.
     * @param dataRole Data role of the USB device.
     * @param mode USB device mode.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    QueryPort([out] int portId, [out] int powerRole, [out] int dataRole, [out] int mode);

    /**
     * @brief Binds a subscriber.
     *
     * @param subscriber Subscriber to bind. For details, see {@link IUsbdSubscriber}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    BindUsbdSubscriber([in] IUsbdSubscriber subscriber);

    /**
     * @brief Unbinds a subscriber.
     *
     * @param subscriber Subscriber to unbind. For details, see {@link IUsbdSubscriber}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    UnbindUsbdSubscriber([in] IUsbdSubscriber subscriber);

    /**
     * @brief Registers a callback for isochronous bulk transfer.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param cb Callback for isochronous bulk transfer. For details, see {@link IUsbdBulkCallback}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    RegBulkCallback([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] IUsbdBulkCallback cb);

    /**
     * @brief Unregisters the callback for isochronous bulk transfer.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    UnRegBulkCallback([in] struct UsbDev dev, [in] struct UsbPipe pipe);

    /**
     * @brief Reads data during isochronous bulk transfer.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param ashmem Shared memory used to store the read data.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    BulkRead([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] Ashmem ashmem);

    /**
     * @brief Writes data during isochronous bulk transfer.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     * @param ashmem Shared memory used to store the written data.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    BulkWrite([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] Ashmem ashmem);

    /**
     * @brief Cancels the isochronous bulk transfer. The read and write operations on the current USB interface will be cancelled.
     *
     * @param dev USB device address. For details, see {@link UsbDev}.
     * @param pipe USB device pipe. For details, see {@link UsbPipe}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a non-0 value if the operation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    BulkCancel([in] struct UsbDev dev, [in] struct UsbPipe pipe);
}
/** @} */
