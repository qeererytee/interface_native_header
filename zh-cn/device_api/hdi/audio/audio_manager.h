/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Audio模块接口定义
 *
 * 音频接口涉及自定义类型、驱动加载接口、驱动适配器接口、音频播放（render）接口、音频录音（capture）接口等
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file audio_manager.h
 *
 * @brief Audio适配器管理及加载的接口定义文件
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H

#include "audio_types.h"
#include "audio_adapter.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief AudioManager音频适配器管理接口
 *
 * 按照音频服务下发的音频适配器（声卡）描述符加载一个具体的音频适配器驱动程序
 *
 * @see AudioAdapter
 * @since 1.0
 * @version 1.0
 */
struct AudioManager {
    /**
     * @brief 获取音频驱动中支持的所有适配器的列表
     *
     * @param manager 待操作的音频管理接口对象
     * @param descs 获取到的音频适配器列表保存到descs中
     * @param size 获取到的音频适配器列表的长度保存到size中
     * @return 成功返回值0，失败返回负值
     * @see LoadAdapter
     */
    int (*GetAllAdapters)(struct AudioAdapterManager *manager, struct AudioAdapterDescriptor **descs, int *size);

    /**
     * @brief 加载一个音频适配器（声卡）的驱动
     *
     * 加载一个具体的音频驱动，例如usb驱动，在具体实现中可能加载的是一个动态链接库（*.so）
     *
     * @param manager 待操作的音频管理接口对象
     * @param desc 待加载的音频适配器描述符
     * @param adapter 获取的音频适配器接口的对象实例保存到adapter中
     * @return 成功返回值0，失败返回负值
     * @see GetAllAdapters
     * @see UnloadAdapter
     */
    int (*LoadAdapter)(struct AudioAdapterManager *manager, const struct AudioAdapterDescriptor *desc,
                    struct AudioAdapter **adapter);

    /**
     * @brief 卸载音频适配器（声卡）的驱动
     *
     * @param manager 待操作的音频管理接口对象
     * @param adapter 待卸载的音频适配器接口的对象
     * @see LoadAdapter
     */
    void (*UnloadAdapter)(struct AudioAdapterManager *manager, struct AudioAdapter *adapter);

    /**
     * @brief 释放音频管理接口对象
     *
     * @param 待操作的音频管理接口对象
     * @return 成功返回ture，失败返回false
     */
    bool (*ReleaseAudioManagerObject)(struct AudioManager *object);
};
/**
 * @brief 获取音频适配器管理接口的操作函数列表，详情参考{@link AudioManager}
 *
 * @return 成功返回一个音频适配器管理接口的对象，失败返回NULL
 */
struct AudioManager *GetAudioManagerFuncs(void);

#endif /* AUDIO_MANAGER_H */
/** @} */
