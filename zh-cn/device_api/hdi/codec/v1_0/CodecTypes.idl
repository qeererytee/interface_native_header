/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Codec
 * @{
 *
 * @brief Codec模块接口定义。
 *
 * Codec模块涉及自定义类型、音视频编解码组件初始化、参数设置、数据的轮转和控制等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file CodecTypes.idl
 *
 * @brief Codec模块接口定义中使用的自定义数据类型。
 *
 * Codec模块接口定义中使用的自定义数据类型，包括编解码类型、音视频参数、buffer定义等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Codec模块接口定义中使用的自定义数据类型的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.codec.v1_0;
sequenceable OHOS.HDI.DISPLAY.BufferHandleParcelable;

/**
 * @brief 枚举编解码的类型。
 */
enum CodecType {
    VIDEO_DECODER,    /**< 视频解码类型。 */
    VIDEO_ENCODER,    /**< 视频编码类型。 */
    AUDIO_DECODER,    /**< 音频解码类型。 */
    AUDIO_ENCODER,    /**< 音频编码类型。 */
    INVALID_TYPE,     /**< 无效类型。 */
};

/**
 * @brief 枚举音视频编解码组件类型。
 */
enum AvCodecRole {
    MEDIA_ROLETYPE_IMAGE_JPEG = 0,           /**< 图像JPEG媒体类型。 */
    MEDIA_ROLETYPE_VIDEO_AVC,                /**< 视频H.264媒体类型。 */
    MEDIA_ROLETYPE_VIDEO_HEVC,               /**< 视频H.265媒体类型。 */
    MEDIA_ROLETYPE_AUDIO_FIRST = 0x10000,    /**< 音频编解码器类型。 */
    MEDIA_ROLETYPE_AUDIO_AAC = 0x10000,      /**< 音频AAC媒体类型。 */
    MEDIA_ROLETYPE_AUDIO_G711A,              /**< 音频G711A媒体类型。 */
    MEDIA_ROLETYPE_AUDIO_G711U,              /**< 音频G711U媒体类型。 */
    MEDIA_ROLETYPE_AUDIO_G726,               /**< 音频G726媒体类型。 */
    MEDIA_ROLETYPE_AUDIO_PCM,                /**< 音频PCM媒体类型。 */
    MEDIA_ROLETYPE_AUDIO_MP3,                /**< 音频MP3媒体类型。 */
    MEDIA_ROLETYPE_INVALID,                  /**< 无效媒体类型。 */
};

/**
 * @brief 枚举Codec规格。
 */
enum Profile {
    INVALID_PROFILE = 0,             /**< 无效的规格。 */
    AAC_LC_PROFILE = 0x1000,         /**< AAC低复杂度规格。 */
    AAC_MAIN_PROFILE,                /**< AAC主规格。 */
    AAC_HE_V1_PROFILE,               /**< AAC高效率和频带重现规格，又称为HEAAC、AAC+、或者AACPlusV1。 */
    AAC_HE_V2_PROFILE,               /**< AAC高效率和频带重现以及变量立体声规格，又称为AAC++或者AACPlusV2。 */
    AAC_LD_PROFILE,                  /**< AAC低延迟规格。 */
    AAC_ELD_PROFILE,                 /**< AAC增强型低延迟规格。 */
    AVC_BASELINE_PROFILE = 0x2000,   /**< H.264低规格。 */
    AVC_MAIN_PROFILE,                /**< H.264主规格。 */
    AVC_HIGH_PROFILE,                /**< H.264高规格。 */
    HEVC_MAIN_PROFILE = 0x3000,      /**< H.265主规格。 */
    HEVC_MAIN_10_PROFILE,            /**< H.265 10比特主规格。 */
};

/**
 * @brief 枚举播放能力。
 */
enum CodecCapsMask {
    CODEC_CAP_ADAPTIVE_PLAYBACK = 0x1,    /**< 自适应播放能力。 */
    CODEC_CAP_SECURE_PLAYBACK = 0x2,      /**< 安全播放能力。 */
    CODEC_CAP_TUNNEL_PLAYBACK = 0x4,      /**< 通道播放能力。 */
    CODEC_CAP_MULTI_PLANE = 0x10000,      /**< 视频图像平面/音频通道平面能力。 */
};

/**
 * @brief 枚举音频采样率。
 */
enum AudioSampleRate {
    AUD_SAMPLE_RATE_8000 = 8000,      /**< 8K采样率。 */
    AUD_SAMPLE_RATE_12000 = 12000,    /**< 12K采样率。 */
    AUD_SAMPLE_RATE_11025 = 11025,    /**< 11.025K采样率。 */
    AUD_SAMPLE_RATE_16000 = 16000,    /**< 16K采样率。 */
    AUD_SAMPLE_RATE_22050 = 22050,    /**< 22.050K采样率。 */
    AUD_SAMPLE_RATE_24000 = 24000,    /**< 24K采样率。 */
    AUD_SAMPLE_RATE_32000 = 32000,    /**< 32K采样率。 */
    AUD_SAMPLE_RATE_44100 = 44100,    /**< 44.1K采样率。 */
    AUD_SAMPLE_RATE_48000 = 48000,    /**< 48K采样率。 */
    AUD_SAMPLE_RATE_64000 = 64000,    /**< 64K采样率。 */
    AUD_SAMPLE_RATE_96000 = 96000,    /**< 96K采样率。 */
    AUD_SAMPLE_RATE_INVALID,          /**< 无效采样率。 */
};

/**
 * @brief 枚举音频采样格式。
 *
 * 对于平面格式的采样格式，每个声道的数据是独立存储在data中;
 * 对于打包格式的采样格式，只使用第一个data，每个声道的数据是交错存储的。
 */
enum AudioSampleFormat {
    AUDIO_SAMPLE_FMT_U8 = 0,        /**< 无符号8位整型，打包格式。 */
    AUDIO_SAMPLE_FMT_S16,           /**< 带符号16位整型, 打包格式。 */
    AUDIO_SAMPLE_FMT_S32,           /**< 带符号32位整型, 打包格式。 */
    AUDIO_SAMPLE_FMT_FLOAT,         /**< 浮点型, 打包格式。 */
    AUDIO_SAMPLE_FMT_DOUBLE,        /**< 双精度浮点型, 打包格式。 */
    AUDIO_SAMPLE_FMT_U8P,           /**< 无符号8位整型, 平面格式。 */
    AUDIO_SAMPLE_FMT_S16P,          /**< 带符号16位整型, 平面格式。 */
    AUDIO_SAMPLE_FMT_S32P,          /**< 带符号32位整型, 平面格式。 */
    AUDIO_SAMPLE_FMT_FLOATP,        /**< 浮点型, 平面格式。 */
    AUDIO_SAMPLE_FMT_DOUBLEP,       /**< 双精度浮点型, 平面格式。 */
    AUDIO_SAMPLE_FMT_INVALID,       /**< 无效采样格式。 */
};

/**
 * @brief 枚举编解码处理模式。
 */
enum CodecProcessMode {
    PROCESS_BLOCKING_INPUT_BUFFER = 0x1,         /**< 同步模式输入buffer。 */
    PROCESS_BLOCKING_OUTPUT_BUFFER = 0x2,        /**< 同步模式输出buffer。 */
    PROCESS_BLOCKING_CONTROL_FLOW = 0x4,         /**< 同步模式控制流。 */
    PROCESS_NONBLOCKING_INPUT_BUFFER = 0x100,    /**< 异步模式输入buffer。 */
    PROCESS_NONBLOCKING_OUTPUT_BUFFER = 0x200,   /**< 异步模式输出buffer。 */
    PROCESS_NONBLOCKING_CONTROL_FLOW = 0x400,    /**< 异步模式控制流。 */
};

/**
 * @brief 枚举共享内存类型。
 */
enum ShareMemTypes {
    READ_WRITE_TYPE = 0x1,   /**< 可读可写的共享内存类型。 */
    READ_ONLY_TYPE = 0x2,    /**< 只读的共享内存类型。 */
};

/**
 * @brief 枚举比特率类型。
 */
enum BitRateMode {
    BIT_RATE_MODE_INVALID,               /**< 定义的一个无效值。 */
    BIT_RATE_MODE_VBR,                   /**< 可变比特率。 */
    BIT_RATE_MODE_CBR,                   /**< 恒定比特率。 */
    BIT_RATE_MODE_CQ,                    /**< 恒定质量。 */
    BIT_RATE_MODE_VCBR,                  /**< 受约束的可变位速率。 */
    BIT_RATE_MODE_ABR,                   /**< 平均比特率。 */
};

/**
* @brief 对齐结构定义。
 */
struct Alignment {
    int widthAlignment;  /**< 宽的对齐值。 */
    int heightAlignment; /**< 高的对齐值。 */
};

/**
 * @brief 矩形的定义。
 */
struct Rect {
    int width;  /**< 矩形的宽。 */
    int height; /**< 矩形的高。 */
};

/**
 * @brief 取值范围的定义。
 */
struct RangeValue {
    int min; /**< 最小值。 */
    int max; /**< 最大值。 */
};

/**
 * @brief 定义视频编解码能力。
 */
struct CodecVideoPortCap {
    struct Rect minSize;                  /**< 支持的最小分辨率。 */
    struct Rect maxSize;                  /**< 支持的最大分辨率。 */
    struct Alignment whAlignment;         /**< 宽高对齐值。 */
    struct RangeValue blockCount;         /**< 支持的块数量范围。 */
    struct RangeValue blocksPerSecond;    /**< 每秒可处理的块数量范围。 */
    struct Rect blockSize;                /**< 支持的块大小。 */
    int[] supportPixFmts;                 /**< 支持的像素格式，详见{@link Display中display_type.h定义的PixeFormat}。 */
    enum BitRateMode[] bitRatemode;       /**< 传输速率模式，有恒定的，有变化的等几种模式。详见{@link BitRateMode}。 */
    struct RangeValue frameRate;　　　　　 /**< 帧率的范围。 */
    int[] measuredFrameRate;              /**< 实测的帧率。 */
};

/**
 * @brief 定义音频编解码能力。
 */
struct CodecAudioPortCap {
    int[] sampleFormats;    /**< 支持的音频采样格式，详见{@link AudioSampleFormat}。 */
    int[] sampleRate;       /**< 支持的音频采样率，详见{@link AudioSampleRate}。 */
    int[] channelLayouts;   /**< 支持的通道格式类型、单通道、平衡声道、3D立体声道等。 */
    int[] channelCount;     /**< 支持的音频通道数。 */
};

/**
 * @brief 定义音视频编解码能力。
 */
struct PortCap {
    struct CodecVideoPortCap video;           /**< 视频编解码能力。 */
    struct CodecAudioPortCap audio;           /**< 音频编解码能力。 */
};

/**
 * @brief 定义组件的版本信息。
 */
struct OmxVerType {
    unsigned char nVersionMajor;        /**< 主要版本访问元件。 */
    unsigned char nVersionMinor;        /**< 次要版本访问元件。 */
    unsigned char nRevision;            /**< 修正版本访问元件。 */
    unsigned char nStep;                /**< 步骤版本访问元件。 */
};

/**
 * @brief 定义组件的版本信息。
 */
union OMX_VERSIONTYPE {
    struct OmxVerType s;          /**< 组件的版本信息。 */
    unsigned int nVersion;        /**< 32位值，使访问版本在单个字大小复制/比较操作中轻松完成。 */
};

/**
 * @brief 定义Codec编解码能力。
 */
struct CodecCompCapability {
    enum AvCodecRole role;                     /**< 媒体类型。 */
    enum CodecType type;                       /**< 编解码类型。 */
    String compName;                           /**< 编解码组件名称。 */
    int[] supportProfiles;                     /**< 支持的profiles，详见{@link Profile}。 */
    int maxInst;                               /**< 最大实例。 */
    boolean isSoftwareCodec;                   /**< 软件编解码还是硬件编解码。 */
    int processModeMask;                       /**< 编解码处理模式掩码，详见{@link CodecProcessMode}。 */
    unsigned int capsMask;                     /**< 编解码播放能力掩码，详见{@link CodecCapsMask}。 */
    struct RangeValue bitRate;                 /**< 支持的码率范围。 */
    struct PortCap port;                       /**< 支持的音视频编解码能力。 */
};

/**
 * @brief Codec buffer信息的定义。
 */
struct OmxCodecBuffer {
    unsigned int bufferId;               /**< buffer ID。 */
    unsigned int size;                   /**< 结构体大小。 */
    union OMX_VERSIONTYPE version;       /**< 组件版本信息。 */
    unsigned int bufferType;             /**< buffer类型,详见{@link CodecBufferType}。 */
    BufferHandleParcelable bufferhandle; /**< 编码或者解码使用的bufferhandle，详见{@link BufferHandleParcelable}。 */
    FileDescriptor fd;                   /**< 匿名共享内存文件描述符。 */
    unsigned int allocLen;               /**< 申请的buffer大小。 */
    unsigned int filledLen;              /**< 填充的buffer大小。 */
    unsigned int offset;                 /**< 有效数据从缓冲区开始的起始偏移量。 */
    FileDescriptor fenceFd;              /**< fence fd。 */
    enum ShareMemTypes type;             /**< 共享内存类型。 */
    long pts;                            /**< 缓冲区第一个逻辑样本时间戳。 */
    unsigned int flag;                   /**< 缓冲区特定标志。 */
};

/**
 * @brief 枚举组件状态。
 */
enum OMX_EVENTTYPE {
    OMX_EventCmdComplete,                    /**< 组件已成功完成命令。 */ 
    OMX_EventError,                          /**< 组件已检测到错误情况。 */ 
    OMX_EventMark,                           /**< 组件已检测到缓冲区标记。 */ 
    OMX_EventPortSettingsChanged,            /**< 组件报告端口设置更改。 */ 
    OMX_EventBufferFlag,                     /**< 组件已检测到EOS。 */ 
    OMX_EventResourcesAcquired,              /**< 组件已被授予资源，并正在自动启动从OMX_StateWaitForResources状态更改为OMX_StateIdle。 */ 
    OMX_EventComponentResumed,               /**< 组件回收由于重新占用的资源。 */ 
    OMX_EventDynamicResourcesAvailable,      /**< 组件已获取此前不可用的动态资源。 */ 
    OMX_EventPortFormatDetected,             /**< 组件已经检测出数据格式。 */ 
    OMX_EventKhronosExtensions = 0x6F000000, /**< 用于引入Khronos标准扩展的保留区域。 */ 
    OMX_EventVendorStartUnused = 0x7F000000, /**< 用于引入供应商扩展的预留区域。 */
    OMX_EventMax = 0x7FFFFFFF,               /**< 枚举的最大值。 */
};

/**
 * @brief 枚举ICodecComponent中SendCommand接口的cmd参数。
 */
enum OMX_COMMANDTYPE
{
    OMX_CommandStateSet,                        /**< 更改组件状态。 */
    OMX_CommandFlush,                           /**< 刷新组件的数据队列。 */
    OMX_CommandPortDisable,                     /**< 禁用组件上的端口。 */
    OMX_CommandPortEnable,                      /**< 启用组件上的端口。 */
    OMX_CommandMarkBuffer,                      /**< 标记组件/缓冲区以进行观察。 */
    OMX_CommandKhronosExtensions = 0x6F000000,  /**< 用于引入Khronos标准扩展的保留区域。 */
    OMX_CommandVendorStartUnused = 0x7F000000,  /**< 用于引入供应商扩展的预留区域。 */
    OMX_CommandMax = 0x7FFFFFFF                 /**< 枚举的最大值。 */
};

/**
 * @brief 更改组件状态。
 */
enum OMX_STATETYPE
{
    OMX_StateInvalid,                           /**< 组件已检测到其内部数据结构已损坏，以至于无法正确确定其状态。 */
    OMX_StateLoaded,                            /**< 组件已加载，但尚未完成初始化。ICodecComponent.SetParameter
                                                     和ICodecComponent.GetParameter是允许发送到处于此状态的组件的唯一有效函数。 */
    OMX_StateIdle,                              /**< 组件初始化已成功完成，组件已准备好启动。*/
    OMX_StateExecuting,                         /**< 组件已接受启动命令并正在处理数据（如果数据可用）。 */
    OMX_StatePause,                             /**< 组件已收到暂停命令。 */
    OMX_StateWaitForResources,                  /**< 组件正在等待资源，无论是在抢占之后还是在获得请求的资源之前。*/                                                                     
    OMX_StateKhronosExtensions = 0x6F000000,    /**< 用于引入Khronos标准扩展的保留区域。 */ 
    OMX_StateVendorStartUnused = 0x7F000000,    /**< 用于引入供应商扩展的预留区域。 */
    OMX_StateMax = 0x7FFFFFFF                   /**< 枚举最大值。 */
};

/**
 * @brief 表示端口供应商在两个端口之间建立隧道时的首选项。
 */
enum OMX_BUFFERSUPPLIERTYPE
{
    OMX_BufferSupplyUnspecified = 0,                   /**< 提供缓冲区的端口未指定，或不指定。 */                                                           
    OMX_BufferSupplyInput,                             /**< 为输入端口提供缓冲区。 */
    OMX_BufferSupplyOutput,                            /**< 为输出端口提供缓冲区。  */
    OMX_BufferSupplyKhronosExtensions = 0x6F000000,    /**< 用于引入Khronos标准扩展的保留区域。 */ 
    OMX_BufferSupplyVendorStartUnused = 0x7F000000,    /**< 用于引入供应商扩展的预留区域。 */
    OMX_BufferSupplyMax = 0x7FFFFFFF                   /**< 枚举的最大值。 */
};

/**
 * @brief 用于将数据从输出端口传递到输入端口。
 */
struct OMX_TUNNELSETUPTYPE {
    unsigned int nTunnelFlags;                /**< 隧道的位标志。 */
    enum OMX_BUFFERSUPPLIERTYPE eSupplier;    /**< 供应商首选项。 */
};

/**
 * @brief 定义了组件信息，包含组件名，UUID， 组件版本以及spec版本。
 */
struct CompVerInfo {
    String compName;                    /**< 组件名称。 */
    unsigned char[] compUUID;           /**< 组件的UUID标识符。 */
    union OMX_VERSIONTYPE compVersion;  /**< OMX组件版本信息。 */
    union OMX_VERSIONTYPE specVersion;  /**< 构建组件所依据的规范的版本信息。 */
};

/**
 * @brief 定义事件上报信息。
 */
struct EventInfo {
    long appData;                /**< 设置回调时给入的上层实例。 */
    unsigned int data1;          /**< Error类型,可能是portIndex或者其它数据。 */
    unsigned int data2;          /**< 事件上报携带的数据2。 */
    byte[] eventData;            /**< 事件上报携带的数据信息。 */
};
/** @} */